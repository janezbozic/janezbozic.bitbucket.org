
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function zapolniMesta(){
	generirajPodatke(0);
	generirajPodatke(1);
	generirajPodatke(2);
} 
 
function generirajPodatke(stPacienta) {
	
	$("#preberiObstojeciEHR").html("<option value=\"\"></option>");
    
    sessionId = getSessionId();
  
    var ehrId;
    var ime, priimek, datumRojstva, spol, visina, teza;
        
        if (stPacienta==0){
                ime = "Stephen";
                priimek = "Curry";
                datumRojstva = "1988-03-14" + "T00:00:00.000Z";
                spol = "m";
                visina = 190;
                teza = 86;
        }
        else if (stPacienta==1){
                ime = "Megan";
                priimek = "Fox";
                datumRojstva = "1986-05-16" + "T00:00:00.000Z";
                spol = "ž";
                visina = 163;
                teza = 52;
        }
        else if (stPacienta==2){
                ime = "Donald";
                priimek = "Trump";
                datumRojstva = "1946-06-14" + "T00:00:00.000Z";
                spol = "m";
                visina = 190;
                teza = 109;
        }
  
	     if (spol=="m") spol = "MALE";
          else spol = "FEMALE";
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "height", value: visina}, {key: "weight", value: teza}, {key: "gender", value: spol}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
            			$("#preberiObstojeciEHR").append('<option value="' + ehrId +'">' + ime + ' ' + priimek + '</option>');
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
		
		//alert("I am an alert box!");
}

function izpisiBolnika(){
	
	var selectedValue = document.getElementById("preberiObstojeciEHR").selectedIndex;
	var ehrId = document.getElementById("preberiObstojeciEHR").options[selectedValue].value;
	
	$("#preberiEHRid").val(ehrId);
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function dodajBolnika(){
     
    sessionId = getSessionId();
  
    var ehrId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajStarost").val() + "T00:00:00.000Z";
    var spol = $("#kreirajSpol").val();
    var visina = $("#kreirajVisino").val();
    var teza = $("#kreirajTezo").val();
  
  
	if (!ime || !priimek || !visina || !teza || !spol || !datumRojstva || ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0 
	|| !(spol.toLocaleLowerCase() == "m" || spol.toLocaleLowerCase() == "ž") || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
	    if (spol.toLowerCase()=="m") spol = "MALE";
          else spol = "FEMALE";
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "height", value: visina}, {key: "weight", value: teza}, {key: "gender", value: spol}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	
	}
	
	return ehrId;
}

function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#izpisiIme").val(party.firstNames);
				$("#izpisiPriimek").val(party.lastNames);
				$("#izpisiStarost").val(party.dateOfBirth);
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}